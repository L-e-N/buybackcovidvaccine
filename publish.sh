#!/bin/bash

# This script generates html pages from .md files, header and footer.

# generates correct html files, including header and footer.
gen_html_file () {

md_file=$1
html_file=$(echo $md_file | sed -e "s/.md/.html/g")
rm $html_file

# generate html content
pandoc $md_file -o $md_file.TEMP

# include header and footer
cat ./header.html >> $html_file
cat $md_file.TEMP >> $html_file
cat ./footer.html >> $html_file

# remove temp file
rm $md_file.TEMP
}

# replaces LANGUAGES in header by links to available languages.
gen_languages () {

lang_list=""
href_model='<a href="../LANG/index.html"> LANG </a> '

for i in ./*/ ; do
    echo $i #debug
    lang=$(echo $i | sed -e "s|\./||g" | sed -e "s|/||g")
    echo $lang
    href=$(echo $href_model | sed -e "s|LANG|$lang|g")
    echo $href
    lang_list=$lang_list$href
    echo $lang_list
done

cp ./header_template.html ./header.html
sed -i -e "s|LANGUAGES|$lang_list|g" ./header.html

}

# generate languages header
gen_languages
# generate html pages
for md_file in ./*/*.md ; do
    echo $md_file
    gen_html_file $md_file
done

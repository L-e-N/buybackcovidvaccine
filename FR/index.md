Financement mondial pour le rachat du vaccin anti-Covid-19
=======================================================

Depuis le début de l'année 2020, la pandémie de Covid-19 bloque la planète entière.

Des laboratoires cherchent jours et nuits à créer un traitement ou un vaccin contre cette maladie.
Nous pensons que ce vaccin doit être disponible pour le plus grand nombre et au moindre coût.
Nous pensons que la formule de ce vaccin, lorsqu'elle sera trouvée, doit être versée au domaine public.
En effet, si la formule est versée au domaine public, nous sommes assurés que les laboratoires du monde entier pourront le fabriquer.
Ceci faciliterait grandement l'accès de chaque humain au vaccin, autant pour la *disponibilité* du vaccin que pour son *prix*.
Nous pensons qu'un laboratoire privé qui trouverait ce vaccin ne devrait pas conserver le monopole de sa fabrication. 


## Une première solution :

<!-- source : https://www.who.int/fr/news-room/detail/24-04-2020-global-leaders-unite-to-ensure-everyone-everywhere-can-access-new-vaccines-tests-and-treatments-for-covid-19 --> 
<!-- source2: https://www.who.int/fr/news-room/detail/12-02-2020-world-experts-and-funders-set-priorities-for-covid-19-research --> 
<!-- source3 : https://www.france24.com/fr/20200424-covid-19-l-oms-mobilise-le-monde-sans-les-%C3%A9tats-unis-pour-un-acc%C3%A8s-universel-aux-vaccins -->

Le 24 avril, l'OMS a lancé une [initiative](https://www.who.int/news-room/detail/24-04-2020-global-leaders-unite-to-ensure-everyone-everywhere-can-access-new-vaccines-tests-and-treatments-for-covid-19) pour accélérer la recherche autour des traitements et des vaccins contre le Covid-19, et leur disponibilité mondiale.
Cette initiative est soutenue par les dirigeants de nombreux pays, à l'exception notable des Etats-Unis et de la Chine.

Il est souhaitable que ces pays, ces laboratoires ayant collaboré décident de rendre publique la formule du vaccin.
Ceci a déjà été fait en 1955 pour le vaccin contre la poliomyélite.
De même, le gel hydroalcoolique est dans le domaine public depuis le début.[^TCrouzet]
C'est la voie légale la plus directe pour assurer la disponibilité mondiale du médicament.

[^TCrouzet]: [Covid-19 : Pour un vaccin libre de droits, T.Crouzet](./article.html)

Mais ce n'est pas certain. Les Etats-Unis et la Chine, qui ne soutiennent pas cette initiative, peuvent décider de prioriser l'usage de ce vaccin pour leur population.
De même, le laboratoire qui l'aurait découvert peut profiter de sa position de monopole pour vendre ce vaccin bien au-dessus de son coût de production.

## Notre proposition :

Si le laboratoire ayant trouvé le vaccin ne veut pas le verser au domaine public, nous proposons de financer mondialement le rachat de ce vaccin, afin de le verser au domaine public. Une ONG pourrait concentrer des paiements et des promesses venant d'Etats, d'entreprises et de particuliers, pour payer le prix demandé par le laboratoire.

### Quelles conséquences ?

* Tous les laboratoires pourraient fabriquer et commercialiser ce vaccin. Le prix de vente couvrirait les coûts de fabrication et une marge, mais n'aurait pas le surcoût lié au monopole du vaccin.
* Le laboratoire qui aurait trouvé le vaccin continuerait à le fabriquer, mais il serait en concurrence avec les autres laboratoires.
* Tous les laboratoires pourraient travailler à améliorer la formule.

### Quel prix ?

Nous n'en savons rien. Le prix devrait être suffisant pour couvrir les frais liés à la recherche, et compenser une partie du manque à gagner pour le laboratoire. Cela monterait sans doute à plusieurs milliards de dollars.


## Est-ce faisable ?

Nous ne savons pas si une telle opération a déjà été menée à bien. 
Les brevets portant sur les médicaments passent dans le domaine public 20 ans après leur délivrance, avec un certificat complémentaire de protection de quelques années.
Nous pensons qu'aujourd'hui, il est préférable de ne pas attendre.


## Comment aider ?

Nous ne sommes pas des experts dans ce domaine, mais nous pensons que cette possibilité doit être explorée. Pour nous aider, partagez l'information :

* Informez des laboratoires et des ONG concernés de cette idée
* Si vous le pouvez, traduisez cette page dans votre langue natale sur [Framagit](https://framagit.org/matograine/buybackcovidvaccine).

* Si vous avez une expertise concernant cette idée, vous pouvez nous contacter : contact [AT] buybackcovidvaccine [POINT] com

### Qui sommes-nous ?

Nous sommes deux citoyens français, T.Negre et T.Crouzet. Nous espérons que notre idée sera reprise et développée par des personnes du monde entier.





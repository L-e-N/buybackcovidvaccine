This repo is here to help translation of the [buybackcovidvaccine.com](https://buybackcovidvaccine.com) webpage.

Contribution is helpful for translation and eventual corrections.

If there is no directory for your language, please :

* fork the project, 
* create a dir for your language,
* translate the **index.md** and **article.md** file. 
* Then make a merge request to `master`.

If you prefer writing directly in html, please do.

I will generate the html pages from the markdown files.
